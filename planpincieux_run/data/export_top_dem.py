import rasterio
import numpy as np
import matplotlib.pyplot as plt
from pyproj import Transformer

# Replace 'your_geotiff_file.tif' with the path to your GeoTIFF file
tiff_file = '/media/martin/data/ENS/3A_MS2SC/stage_M2/data_planpincieux/pp_data/top_dem.tif'
save_txt_file='/media/martin/data/ENS/3A_MS2SC/stage_M2/data_planpincieux/pp_data/top_dem.txt'

replacement_value=-9999



# Open the TIFF file using rasterio
with rasterio.open(tiff_file) as src:
    # Read the data into a NumPy array
    elevation_data = src.read(1)  # Reading the first band (assuming it's elevation)

    # Get the affine transformation
    transform = src.transform

    transformer = Transformer.from_crs(src.crs,"EPSG:32632")

    # Get the dimensions of the raster
    rows, cols = elevation_data.shape

    # Create arrays for the coordinates
    x_coords = np.zeros((rows, cols))
    y_coords = np.zeros((rows, cols))

    # Calculate coordinates for each pixel
    for row in range(rows):
        for col in range(cols):
            x, y = rasterio.transform.xy(transform, row, col)
            x,y=transformer.transform(x,y)
            x_coords[row, col] = x
            y_coords[row, col] = y


            # if row+col<28 or row<5:
            #     elevation_data[row,col]=replacement_value
            # if elevation_data[row,col]==0:
            #     elevation_data[row,col]=replacement_value


    # Now x_coords and y_coords contain the geographic coordinates of each pixel
    print("Elevation data shape:", elevation_data.shape)
    print("X coordinates shape:", x_coords.shape)
    print("Y coordinates shape:", y_coords.shape)

    # Plot the elevation data
    plt.figure(figsize=(10, 10))
    plt.imshow(elevation_data, cmap='terrain')
    plt.colorbar(label='Elevation')
    plt.title("Elevation Data")
    plt.show()


f=open(save_txt_file,"w")
for row in range(rows):
    for col in range(cols):
        f.write(str(x_coords[row,col])+";"+str(y_coords[row,col])+";"+str(elevation_data[row,col])+"\n")

f.close()


# Example: Access coordinates of a specific point (e.g., row=100, col=200)
# row, col = 100, 200
# print(f"Coordinates of point ({row}, {col}): X={x_coords[row, col]}, Y={y_coords[row, col]}")