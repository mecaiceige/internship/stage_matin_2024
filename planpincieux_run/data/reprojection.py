import rasterio
from rasterio.warp import calculate_default_transform, reproject, Resampling
from rasterio.enums import Resampling
import rasterio
import numpy as np
import matplotlib.pyplot as plt
from pyproj import Transformer
import cv2

# Replace these with the paths to your input GeoTIFF files
top_tiff_file = '/media/martin/data/ENS/3A_MS2SC/stage_M2/data_planpincieux/pp_data/top_dem.tif'
thickness_tiff_file = '/media/martin/data/ENS/3A_MS2SC/stage_M2/data_planpincieux/pp_data/bed_dem.tif'

save_txt_file_top_dem='/media/martin/data/ENS/3A_MS2SC/stage_M2/data_planpincieux/pp_data/top_dem.txt'
save_txt_file_bottom_dem='/media/martin/data/ENS/3A_MS2SC/stage_M2/data_planpincieux/pp_data/bottom_dem.txt'
save_txt_contour='/media/martin/data/ENS/3A_MS2SC/stage_M2/data_planpincieux/pp_data/contour.txt'

replacement_value=-9999.0



# Target CRS (EPSG:32632 - WGS 84 / UTM zone 32N)
dst_crs = 'EPSG:32632'

def reproject_geotiff(input_file, output_file, dst_crs):
    # Open the input GeoTIFF file
    with rasterio.open(input_file) as src:
        # Calculate the transform and dimensions of the reprojected raster
        transform, width, height = calculate_default_transform(
            src.crs, dst_crs, src.width, src.height, *src.bounds,resolution=(10, 10))

        # Update the metadata for the new projection
        kwargs = src.meta.copy()
        kwargs.update({
            'crs': dst_crs,
            'transform': transform,
            'width': width,
            'height': height
        })
        print(transform)

        # Create the output file with the updated metadata
        with rasterio.open(output_file, 'w', **kwargs) as dst:
            for i in range(1, src.count + 1):
                # Reproject and write each band
                reproject(
                    source=rasterio.band(src, i),
                    destination=rasterio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=transform,
                    dst_crs=dst_crs,
                    resampling=Resampling.nearest)

    print(f"Reprojected {input_file} to {output_file} with CRS {dst_crs}")

# Reproject both GeoTIFF files
reproject_geotiff(top_tiff_file, top_tiff_file, dst_crs)
reproject_geotiff(thickness_tiff_file,thickness_tiff_file, dst_crs)


with rasterio.open(top_tiff_file) as src:
    elevation_data = src.read(1)
    transform = src.transform
    rows, cols = elevation_data.shape
    x_coords = np.zeros((rows, cols))
    y_coords = np.zeros((rows, cols))
    for row in range(rows):
        for col in range(cols):
            x, y = rasterio.transform.xy(transform, row, col)
            x_coords[row, col] = x
            y_coords[row, col] = y

top_dem=np.array([x_coords,y_coords,elevation_data])
top_dem[2]=np.where(top_dem[2]==0, replacement_value, top_dem[2])


with rasterio.open(thickness_tiff_file) as src:
    elevation_data = src.read(1)
    transform = src.transform
    rows, cols = elevation_data.shape
    x_coords = np.zeros((rows, cols))
    y_coords = np.zeros((rows, cols))
    for row in range(rows):
        for col in range(cols):
            x, y = rasterio.transform.xy(transform, row, col)
            x_coords[row, col] = x
            y_coords[row, col] = y
            if row+col<28 or row<5:
                elevation_data[row,col]=replacement_value
            if elevation_data[row,col]==0:
                elevation_data[row,col]=replacement_value


thickness_dem=np.array([x_coords,y_coords,elevation_data])

bottom_dem=np.zeros(np.shape(thickness_dem[0]))

for row in range(np.shape(bottom_dem)[0]):
    for col in range(np.shape(bottom_dem)[1]):
        if elevation_data[row,col]!=replacement_value:

            x=thickness_dem[0,row,col]
            y=thickness_dem[1,row,col]

            error=10e+15
            row_min=0
            col_min=0

            for  row_2 in range(len(top_dem[0,0])):
                for  col_2 in range(len(top_dem[0,1])):
                    diff=abs(top_dem[0,row_2,col_2]-x)+abs(top_dem[1,row_2,col_2]-y)
                    if diff<error:
                        error=diff
                        row_min=row_2
                        col_min=col_2

            bottom_dem[row,col]=top_dem[2,row_min,col_min]-thickness_dem[2,row,col]

bottom_dem=np.array([thickness_dem[0],thickness_dem[1],np.where(bottom_dem==0,replacement_value,bottom_dem)])



binary_image=np.where(bottom_dem[2]==replacement_value, 0, bottom_dem[2])

binary_image = np.where(binary_image != 0, 255, 0).astype(np.uint8)

contours, _ = cv2.findContours(binary_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
contour_points = contours[0].reshape(-1, 2)

contour=[]

for point in contour_points:
    contour.append([bottom_dem[0,point[1],point[0]],bottom_dem[1,point[1],point[0]]])
contour=np.array(contour)


f=open(save_txt_file_top_dem,"w")
for row in range(np.shape(top_dem[0])[0]):
    for col in range(np.shape(top_dem[0])[1]):
        f.write(str(top_dem[0,row,col])+" "+str(top_dem[1,row,col])+" "+str(top_dem[2,row,col])+"\n")
f.close()

f=open(save_txt_file_bottom_dem,"w")
for row in range(np.shape(bottom_dem[0])[0]):
    for col in range(np.shape(bottom_dem[0])[1]):
        f.write(str(bottom_dem[0,row,col])+" "+str(bottom_dem[1,row,col])+" "+str(bottom_dem[2,row,col])+"\n")
f.close()

f=open(save_txt_contour,"w")
for line in contour:
        f.write(str(line[0])+" "+str(line[1])+"\n")
f.close()



plt.subplot(2, 2, 1)
plt.imshow(np.where(top_dem[2]==replacement_value, np.nan, top_dem[2]),label="top dem")
plt.colorbar()
plt.axis('equal')

plt.subplot(2, 2, 2)
plt.imshow(np.where(thickness_dem[2]==replacement_value, np.nan, thickness_dem[2]),label="thickness")
plt.axis('equal')
plt.colorbar()

plt.subplot(2, 2, 3)
plt.imshow(np.where(bottom_dem[2]==replacement_value, np.nan, bottom_dem[2]),label="bottom_dem")
plt.axis('equal')
plt.colorbar()

plt.subplot(2, 2, 4)
plt.plot(contour_points[:, 0], contour_points[:, 1],label="bottom_dem")
plt.axis('equal')

plt.show()

