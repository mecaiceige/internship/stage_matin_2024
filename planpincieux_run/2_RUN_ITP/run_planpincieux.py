from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import os


path=os.path.dirname(os.path.abspath(__file__))



my_mesh=it.Mesh(path+"/"+"planpincieux.vtu")
my_mesh.invert_mesh()
my_mesh.extract_data()
start_points_list=[[342533.393,5079988.393,2920],[342533.393,5079988.393,2910],[342533.393,5079988.393,2900],[342533.393,5079988.393,2890],[342533.393,5079988.393,2880],[342533.393,5079988.393,2870],[342533.393,5079988.393,2860],[342533.393,5079988.393,2850],
                   [342647.97187,5080030.24320,2917],[342647.97187,5080030.24320,2907],[342647.97187,5080030.24320,2897],[342647.97187,5080030.24320,2887],[342647.97187,5080030.24320,2877],[342647.97187,5080030.24320,2867],[342647.97187,5080030.24320,2857],[342647.97187,5080030.24320,2847],[342647.97187,5080030.24320,2837],
                   [342578.84947,5079917.83902,2900],[342578.84947,5079917.83902,2894],[342578.84947,5079917.83902,2884],[342578.84947,5079917.83902,2874],[342578.84947,5079917.83902,2864],[342578.84947,5079917.83902,2854],[342578.84947,5079917.83902,2844],[342578.84947,5079917.83902,2834],[342578.84947,5079917.83902,2824],[342578.84947,5079917.83902,2814],
                    [342651.43625,5079968.02276,2900],[342651.43625,5079968.02276,2890],[342651.43625,5079968.02276,2880],[342651.43625,5079968.02276,2870],[342651.43625,5079968.02276,2860],[342651.43625,5079968.02276,2850]]


tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],ellipse_factor=5,dt=-0.5,start_acu_level=2900,n=1000,u_min=0.01)
tracker.compute_path(start_points_list)
tracker.time_inversion()
[particule.update({"1":[1]*len(particule["time_step"])}) for particule in tracker.particule]
tracker.time_int_over_path("1")
tracker.compute_rotation_matrix()
tracker.compute_in_particule_coord("e")
tracker.compute_strain("Le")
tracker.compute_deformation_eigval("eigval_ILe")
tracker.compute_flatness_anisotropy("deformation_eigval")
tracker.compute_fractional_anisotropy("deformation_eigval")
tracker.compute_volume_ratio_anisotropy("deformation_eigval")
tracker.compute_relative_anisotropy("deformation_eigval")

plot=it.Plot(cmap="brg",projection_2D=True)
# plot.mesh_full(my_mesh)
plot.mesh(my_mesh)
plot.particule_scalar(tracker.particule,"relative_anisotropy")
plot.cbar()
plot.axis()
plot.show()




