// This a a geo file created using the python script Contour2geo.py // 
Mesh.Algorithm=5; 
// To controle the element size, one can directly modify the lc value in the geo file // 
lc = 150.0 ; 
// Mesh size near the boundary from prescribed value  //
Mesh.CharacteristicLengthFromCurvature = 0; 
Mesh.CharacteristicLengthFromPoints = 1; 
// Give a backgroung field with uniform value for the mesh size  // 
Mesh.CharacteristicLengthExtendFromBoundary = 0; 
Field[1] = MathEval; 
Field[1].F = Sprintf("%g",lc); 
Background Field = 1; 
Point(1) = { 343625.0, 5081475.0, 0.0, lc}; 
Point(2) = { 343576.52, 5081433.0, 0.0, lc}; 
Point(3) = { 343577.32, 5081370.84, 0.0, lc}; 
Point(4) = { 343569.04, 5081311.82, 0.0, lc}; 
Point(5) = { 343522.2, 5081267.98, 0.0, lc}; 
Point(6) = { 343463.66, 5081229.56, 0.0, lc}; 
Point(7) = { 343426.77, 5081184.27, 0.0, lc}; 
Point(8) = { 343424.52, 5081123.36, 0.0, lc}; 
Point(9) = { 343381.31, 5081075.8, 0.0, lc}; 
Point(10) = { 343334.22, 5081108.46, 0.0, lc}; 
Point(11) = { 343305.4, 5081174.06, 0.0, lc}; 
Point(12) = { 343304.05, 5081236.94, 0.0, lc}; 
Point(13) = { 343342.15, 5081291.78, 0.0, lc}; 
Point(14) = { 343374.95, 5081330.49, 0.0, lc}; 
Point(15) = { 343325.64, 5081343.99, 0.0, lc}; 
Point(16) = { 343242.02, 5081331.48, 0.0, lc}; 
Point(17) = { 343185.0, 5081294.0, 0.0, lc}; 
Point(18) = { 343169.73, 5081237.74, 0.0, lc}; 
Point(19) = { 343180.26, 5081171.49, 0.0, lc}; 
Point(20) = { 343193.92, 5081104.13, 0.0, lc}; 
Point(21) = { 343187.88, 5081044.55, 0.0, lc}; 
Point(22) = { 343144.86, 5080999.62, 0.0, lc}; 
Point(23) = { 343123.95, 5080947.78, 0.0, lc}; 
Point(24) = { 343110.95, 5080880.32, 0.0, lc}; 
Point(25) = { 343060.87, 5080830.5, 0.0, lc}; 
Point(26) = { 343005.43, 5080835.06, 0.0, lc}; 
Point(27) = { 342968.44, 5080889.45, 0.0, lc}; 
Point(28) = { 342957.08, 5080951.53, 0.0, lc}; 
Point(29) = { 342970.21, 5081014.04, 0.0, lc}; 
Point(30) = { 343007.13, 5081067.71, 0.0, lc}; 
Point(31) = { 343068.56, 5081073.77, 0.0, lc}; 
Point(32) = { 343091.66, 5081119.79, 0.0, lc}; 
Point(33) = { 343064.15, 5081191.58, 0.0, lc}; 
Point(34) = { 343007.26, 5081230.12, 0.0, lc}; 
Point(35) = { 342936.7, 5081234.42, 0.0, lc}; 
Point(36) = { 342867.55, 5081209.86, 0.0, lc}; 
Point(37) = { 342814.73, 5081162.01, 0.0, lc}; 
Point(38) = { 342769.47, 5081124.24, 0.0, lc}; 
Point(39) = { 342709.51, 5081118.38, 0.0, lc}; 
Point(40) = { 342668.53, 5081066.11, 0.0, lc}; 
Point(41) = { 342624.28, 5081024.9, 0.0, lc}; 
Point(42) = { 342563.12, 5081021.23, 0.0, lc}; 
Point(43) = { 342523.38, 5080970.5, 0.0, lc}; 
Point(44) = { 342534.08, 5080912.9, 0.0, lc}; 
Point(45) = { 342579.24, 5080868.39, 0.0, lc}; 
Point(46) = { 342588.18, 5080803.83, 0.0, lc}; 
Point(47) = { 342551.39, 5080753.01, 0.0, lc}; 
Point(48) = { 342519.6, 5080702.31, 0.0, lc}; 
Point(49) = { 342547.8, 5080648.76, 0.0, lc}; 
Point(50) = { 342588.37, 5080603.47, 0.0, lc}; 
Point(51) = { 342594.66, 5080540.87, 0.0, lc}; 
Point(52) = { 342574.81, 5080474.58, 0.0, lc}; 
Point(53) = { 342538.74, 5080418.97, 0.0, lc}; 
Point(54) = { 342490.13, 5080381.38, 0.0, lc}; 
Point(55) = { 342431.4, 5080367.03, 0.0, lc}; 
Point(56) = { 342367.52, 5080377.46, 0.0, lc}; 
Point(57) = { 342308.72, 5080411.29, 0.0, lc}; 
Point(58) = { 342276.25, 5080464.73, 0.0, lc}; 
Point(59) = { 342274.45, 5080526.58, 0.0, lc}; 
Point(60) = { 342225.07, 5080554.44, 0.0, lc}; 
Point(61) = { 342149.46, 5080540.59, 0.0, lc}; 
Point(62) = { 342100.48, 5080490.31, 0.0, lc}; 
Point(63) = { 342097.59, 5080422.52, 0.0, lc}; 
Point(64) = { 342135.3, 5080366.15, 0.0, lc}; 
Point(65) = { 342175.29, 5080321.64, 0.0, lc}; 
Point(66) = { 342183.39, 5080262.82, 0.0, lc}; 
Point(67) = { 342226.7, 5080218.78, 0.0, lc}; 
Point(68) = { 342227.91, 5080157.22, 0.0, lc}; 
Point(69) = { 342264.89, 5080110.48, 0.0, lc}; 
Point(70) = { 342322.28, 5080072.21, 0.0, lc}; 
Point(71) = { 342371.27, 5080030.45, 0.0, lc}; 
Point(72) = { 342388.39, 5079975.47, 0.0, lc}; 
Point(73) = { 342380.33, 5079910.04, 0.0, lc}; 
Point(74) = { 342364.26, 5079841.31, 0.0, lc}; 
Point(75) = { 342357.41, 5079776.44, 0.0, lc}; 
Point(76) = { 342376.97, 5079722.56, 0.0, lc}; 
Point(77) = { 342427.6, 5079681.58, 0.0, lc}; 
Point(78) = { 342471.02, 5079637.46, 0.0, lc}; 
Point(79) = { 342474.88, 5079576.94, 0.0, lc}; 
Point(80) = { 342502.29, 5079528.38, 0.0, lc}; 
Point(81) = { 342559.93, 5079493.35, 0.0, lc}; 
Point(82) = { 342623.23, 5079459.1, 0.0, lc}; 
Point(83) = { 342667.56, 5079412.91, 0.0, lc}; 
Point(84) = { 342671.51, 5079344.78, 0.0, lc}; 
Point(85) = { 342711.13, 5079325.47, 0.0, lc}; 
Point(86) = { 342760.71, 5079289.77, 0.0, lc}; 
Point(87) = { 342813.74, 5079271.65, 0.0, lc}; 
Point(88) = { 342877.5, 5079273.83, 0.0, lc}; 
Point(89) = { 342921.17, 5079228.77, 0.0, lc}; 
Point(90) = { 342967.27, 5079213.75, 0.0, lc}; 
Point(91) = { 343018.88, 5079244.53, 0.0, lc}; 
Point(92) = { 343068.35, 5079304.75, 0.0, lc}; 
Point(93) = { 343108.05, 5079378.04, 0.0, lc}; 
Point(94) = { 343130.35, 5079448.22, 0.0, lc}; 
Point(95) = { 343129.47, 5079509.36, 0.0, lc}; 
Point(96) = { 343103.08, 5079568.52, 0.0, lc}; 
Point(97) = { 343056.05, 5079613.99, 0.0, lc}; 
Point(98) = { 342996.94, 5079625.38, 0.0, lc}; 
Point(99) = { 342933.38, 5079633.94, 0.0, lc}; 
Point(100) = { 342875.42, 5079670.37, 0.0, lc}; 
Point(101) = { 342835.54, 5079725.77, 0.0, lc}; 
Point(102) = { 342825.93, 5079790.4, 0.0, lc}; 
Point(103) = { 342810.92, 5079845.4, 0.0, lc}; 
Point(104) = { 342762.63, 5079888.26, 0.0, lc}; 
Point(105) = { 342738.46, 5079942.67, 0.0, lc}; 
Point(106) = { 342738.25, 5080006.49, 0.0, lc}; 
Point(107) = { 342755.27, 5080074.68, 0.0, lc}; 
Point(108) = { 342782.81, 5080142.16, 0.0, lc}; 
Point(109) = { 342819.46, 5080198.8, 0.0, lc}; 
Point(110) = { 342873.76, 5080224.9, 0.0, lc}; 
Point(111) = { 342930.78, 5080228.93, 0.0, lc}; 
Point(112) = { 342929.18, 5080285.19, 0.0, lc}; 
Point(113) = { 342920.24, 5080360.11, 0.0, lc}; 
Point(114) = { 342959.87, 5080408.65, 0.0, lc}; 
Point(115) = { 342978.83, 5080459.76, 0.0, lc}; 
Point(116) = { 342975.14, 5080525.41, 0.0, lc}; 
Point(117) = { 343017.44, 5080572.65, 0.0, lc}; 
Point(118) = { 343071.91, 5080561.74, 0.0, lc}; 
Point(119) = { 343113.93, 5080504.51, 0.0, lc}; 
Point(120) = { 343133.01, 5080432.22, 0.0, lc}; 
Point(121) = { 343129.99, 5080359.97, 0.0, lc}; 
Point(122) = { 343114.7, 5080289.96, 0.0, lc}; 
Point(123) = { 343097.21, 5080224.07, 0.0, lc}; 
Point(124) = { 343087.59, 5080164.15, 0.0, lc}; 
Point(125) = { 343095.92, 5080112.07, 0.0, lc}; 
Point(126) = { 343132.21, 5080069.66, 0.0, lc}; 
Point(127) = { 343174.91, 5080024.78, 0.0, lc}; 
Point(128) = { 343179.52, 5079968.58, 0.0, lc}; 
Point(129) = { 343241.16, 5079970.55, 0.0, lc}; 
Point(130) = { 343319.31, 5079971.21, 0.0, lc}; 
Point(131) = { 343380.16, 5079938.79, 0.0, lc}; 
Point(132) = { 343416.98, 5079882.56, 0.0, lc}; 
Point(133) = { 343423.84, 5079812.96, 0.0, lc}; 
Point(134) = { 343408.86, 5079741.95, 0.0, lc}; 
Point(135) = { 343414.67, 5079685.13, 0.0, lc}; 
Point(136) = { 343477.04, 5079658.92, 0.0, lc}; 
Point(137) = { 343526.45, 5079681.69, 0.0, lc}; 
Point(138) = { 343533.93, 5079747.68, 0.0, lc}; 
Point(139) = { 343583.74, 5079770.76, 0.0, lc}; 
Point(140) = { 343628.02, 5079723.55, 0.0, lc}; 
Point(141) = { 343688.54, 5079732.17, 0.0, lc}; 
Point(142) = { 343728.38, 5079781.43, 0.0, lc}; 
Point(143) = { 343747.95, 5079846.96, 0.0, lc}; 
Point(144) = { 343736.79, 5079909.28, 0.0, lc}; 
Point(145) = { 343689.37, 5079953.71, 0.0, lc}; 
Point(146) = { 343676.88, 5080009.24, 0.0, lc}; 
Point(147) = { 343638.25, 5080059.91, 0.0, lc}; 
Point(148) = { 343642.94, 5080099.16, 0.0, lc}; 
Point(149) = { 343695.4, 5080152.74, 0.0, lc}; 
Point(150) = { 343705.19, 5080219.95, 0.0, lc}; 
Point(151) = { 343672.45, 5080277.09, 0.0, lc}; 
Point(152) = { 343626.52, 5080319.84, 0.0, lc}; 
Point(153) = { 343620.29, 5080378.38, 0.0, lc}; 
Point(154) = { 343566.7, 5080361.87, 0.0, lc}; 
Point(155) = { 343576.12, 5080294.95, 0.0, lc}; 
Point(156) = { 343547.03, 5080243.3, 0.0, lc}; 
Point(157) = { 343512.06, 5080198.76, 0.0, lc}; 
Point(158) = { 343517.38, 5080139.18, 0.0, lc}; 
Point(159) = { 343530.17, 5080071.47, 0.0, lc}; 
Point(160) = { 343515.2, 5080004.02, 0.0, lc}; 
Point(161) = { 343464.55, 5079976.95, 0.0, lc}; 
Point(162) = { 343424.55, 5080027.8, 0.0, lc}; 
Point(163) = { 343421.81, 5080087.73, 0.0, lc}; 
Point(164) = { 343380.51, 5080131.44, 0.0, lc}; 
Point(165) = { 343320.61, 5080169.36, 0.0, lc}; 
Point(166) = { 343278.07, 5080214.91, 0.0, lc}; 
Point(167) = { 343277.81, 5080277.14, 0.0, lc}; 
Point(168) = { 343268.31, 5080335.08, 0.0, lc}; 
Point(169) = { 343222.56, 5080378.22, 0.0, lc}; 
Point(170) = { 343209.76, 5080435.22, 0.0, lc}; 
Point(171) = { 343222.92, 5080502.97, 0.0, lc}; 
Point(172) = { 343236.18, 5080570.5, 0.0, lc}; 
Point(173) = { 343223.7, 5080626.86, 0.0, lc}; 
Point(174) = { 343179.09, 5080670.27, 0.0, lc}; 
Point(175) = { 343154.82, 5080725.51, 0.0, lc}; 
Point(176) = { 343161.8, 5080791.16, 0.0, lc}; 
Point(177) = { 343192.15, 5080853.46, 0.0, lc}; 
Point(178) = { 343246.88, 5080874.3, 0.0, lc}; 
Point(179) = { 343297.87, 5080897.45, 0.0, lc}; 
Point(180) = { 343352.01, 5080928.11, 0.0, lc}; 
Point(181) = { 343418.46, 5080920.17, 0.0, lc}; 
Point(182) = { 343424.98, 5080973.56, 0.0, lc}; 
Point(183) = { 343464.85, 5081022.63, 0.0, lc}; 
Point(184) = { 343526.82, 5081025.8, 0.0, lc}; 
Point(185) = { 343570.09, 5081070.61, 0.0, lc}; 
Point(186) = { 343628.74, 5081086.38, 0.0, lc}; 
Point(187) = { 343702.47, 5081067.42, 0.0, lc}; 
Point(188) = { 343772.8, 5081049.41, 0.0, lc}; 
Point(189) = { 343821.24, 5081068.05, 0.0, lc}; 
Point(190) = { 343822.51, 5081125.84, 0.0, lc}; 
Point(191) = { 343757.78, 5081127.49, 0.0, lc}; 
Point(192) = { 343703.39, 5081160.87, 0.0, lc}; 
Point(193) = { 343675.11, 5081221.63, 0.0, lc}; 
Point(194) = { 343678.1, 5081286.95, 0.0, lc}; 
Point(195) = { 343712.81, 5081339.04, 0.0, lc}; 
Point(196) = { 343768.2, 5081372.37, 0.0, lc}; 
Point(197) = { 343831.56, 5081383.24, 0.0, lc}; 
Point(198) = { 343889.83, 5081368.37, 0.0, lc}; 
Point(199) = { 343901.68, 5081363.62, 0.0, lc}; 
Point(200) = { 343775.0, 5081475.0, 0.0, lc}; 
Line(1) = {1,2}; 
Line(2) = {2,3}; 
Line(3) = {3,4}; 
Line(4) = {4,5}; 
Line(5) = {5,6}; 
Line(6) = {6,7}; 
Line(7) = {7,8}; 
Line(8) = {8,9}; 
Line(9) = {9,10}; 
Line(10) = {10,11}; 
Line(11) = {11,12}; 
Line(12) = {12,13}; 
Line(13) = {13,14}; 
Line(14) = {14,15}; 
Line(15) = {15,16}; 
Line(16) = {16,17}; 
Line(17) = {17,18}; 
Line(18) = {18,19}; 
Line(19) = {19,20}; 
Line(20) = {20,21}; 
Line(21) = {21,22}; 
Line(22) = {22,23}; 
Line(23) = {23,24}; 
Line(24) = {24,25}; 
Line(25) = {25,26}; 
Line(26) = {26,27}; 
Line(27) = {27,28}; 
Line(28) = {28,29}; 
Line(29) = {29,30}; 
Line(30) = {30,31}; 
Line(31) = {31,32}; 
Line(32) = {32,33}; 
Line(33) = {33,34}; 
Line(34) = {34,35}; 
Line(35) = {35,36}; 
Line(36) = {36,37}; 
Line(37) = {37,38}; 
Line(38) = {38,39}; 
Line(39) = {39,40}; 
Line(40) = {40,41}; 
Line(41) = {41,42}; 
Line(42) = {42,43}; 
Line(43) = {43,44}; 
Line(44) = {44,45}; 
Line(45) = {45,46}; 
Line(46) = {46,47}; 
Line(47) = {47,48}; 
Line(48) = {48,49}; 
Line(49) = {49,50}; 
Line(50) = {50,51}; 
Line(51) = {51,52}; 
Line(52) = {52,53}; 
Line(53) = {53,54}; 
Line(54) = {54,55}; 
Line(55) = {55,56}; 
Line(56) = {56,57}; 
Line(57) = {57,58}; 
Line(58) = {58,59}; 
Line(59) = {59,60}; 
Line(60) = {60,61}; 
Line(61) = {61,62}; 
Line(62) = {62,63}; 
Line(63) = {63,64}; 
Line(64) = {64,65}; 
Line(65) = {65,66}; 
Line(66) = {66,67}; 
Line(67) = {67,68}; 
Line(68) = {68,69}; 
Line(69) = {69,70}; 
Line(70) = {70,71}; 
Line(71) = {71,72}; 
Line(72) = {72,73}; 
Line(73) = {73,74}; 
Line(74) = {74,75}; 
Line(75) = {75,76}; 
Line(76) = {76,77}; 
Line(77) = {77,78}; 
Line(78) = {78,79}; 
Line(79) = {79,80}; 
Line(80) = {80,81}; 
Line(81) = {81,82}; 
Line(82) = {82,83}; 
Line(83) = {83,84}; 
Line(84) = {84,85}; 
Line(85) = {85,86}; 
Line(86) = {86,87}; 
Line(87) = {87,88}; 
Line(88) = {88,89}; 
Line(89) = {89,90}; 
Line(90) = {90,91}; 
Line(91) = {91,92}; 
Line(92) = {92,93}; 
Line(93) = {93,94}; 
Line(94) = {94,95}; 
Line(95) = {95,96}; 
Line(96) = {96,97}; 
Line(97) = {97,98}; 
Line(98) = {98,99}; 
Line(99) = {99,100}; 
Line(100) = {100,101}; 
Line(101) = {101,102}; 
Line(102) = {102,103}; 
Line(103) = {103,104}; 
Line(104) = {104,105}; 
Line(105) = {105,106}; 
Line(106) = {106,107}; 
Line(107) = {107,108}; 
Line(108) = {108,109}; 
Line(109) = {109,110}; 
Line(110) = {110,111}; 
Line(111) = {111,112}; 
Line(112) = {112,113}; 
Line(113) = {113,114}; 
Line(114) = {114,115}; 
Line(115) = {115,116}; 
Line(116) = {116,117}; 
Line(117) = {117,118}; 
Line(118) = {118,119}; 
Line(119) = {119,120}; 
Line(120) = {120,121}; 
Line(121) = {121,122}; 
Line(122) = {122,123}; 
Line(123) = {123,124}; 
Line(124) = {124,125}; 
Line(125) = {125,126}; 
Line(126) = {126,127}; 
Line(127) = {127,128}; 
Line(128) = {128,129}; 
Line(129) = {129,130}; 
Line(130) = {130,131}; 
Line(131) = {131,132}; 
Line(132) = {132,133}; 
Line(133) = {133,134}; 
Line(134) = {134,135}; 
Line(135) = {135,136}; 
Line(136) = {136,137}; 
Line(137) = {137,138}; 
Line(138) = {138,139}; 
Line(139) = {139,140}; 
Line(140) = {140,141}; 
Line(141) = {141,142}; 
Line(142) = {142,143}; 
Line(143) = {143,144}; 
Line(144) = {144,145}; 
Line(145) = {145,146}; 
Line(146) = {146,147}; 
Line(147) = {147,148}; 
Line(148) = {148,149}; 
Line(149) = {149,150}; 
Line(150) = {150,151}; 
Line(151) = {151,152}; 
Line(152) = {152,153}; 
Line(153) = {153,154}; 
Line(154) = {154,155}; 
Line(155) = {155,156}; 
Line(156) = {156,157}; 
Line(157) = {157,158}; 
Line(158) = {158,159}; 
Line(159) = {159,160}; 
Line(160) = {160,161}; 
Line(161) = {161,162}; 
Line(162) = {162,163}; 
Line(163) = {163,164}; 
Line(164) = {164,165}; 
Line(165) = {165,166}; 
Line(166) = {166,167}; 
Line(167) = {167,168}; 
Line(168) = {168,169}; 
Line(169) = {169,170}; 
Line(170) = {170,171}; 
Line(171) = {171,172}; 
Line(172) = {172,173}; 
Line(173) = {173,174}; 
Line(174) = {174,175}; 
Line(175) = {175,176}; 
Line(176) = {176,177}; 
Line(177) = {177,178}; 
Line(178) = {178,179}; 
Line(179) = {179,180}; 
Line(180) = {180,181}; 
Line(181) = {181,182}; 
Line(182) = {182,183}; 
Line(183) = {183,184}; 
Line(184) = {184,185}; 
Line(185) = {185,186}; 
Line(186) = {186,187}; 
Line(187) = {187,188}; 
Line(188) = {188,189}; 
Line(189) = {189,190}; 
Line(190) = {190,191}; 
Line(191) = {191,192}; 
Line(192) = {192,193}; 
Line(193) = {193,194}; 
Line(194) = {194,195}; 
Line(195) = {195,196}; 
Line(196) = {196,197}; 
Line(197) = {197,198}; 
Line(198) = {198,199}; 
Line(199) = {199,200}; 
Line(200) = {200,1}; 
Curve Loop(1) = {1:200}; 
Plane Surface(1) = {1}; 
Physical Curve(1) = {1:200}; 
Physical Surface(1) = {1}; 
