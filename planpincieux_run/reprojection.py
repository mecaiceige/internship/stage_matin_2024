import rasterio
from rasterio.warp import calculate_default_transform, reproject, Resampling
from rasterio.enums import Resampling
import rasterio
import numpy as np
import matplotlib.pyplot as plt
from pyproj import Transformer
import cv2
import math
from scipy import linalg as la
from tqdm import tqdm
from scipy.interpolate import splprep, splev
plt.close("all")

path=os.path.dirname(os.path.abspath(__file__))
# Replace these with the paths to your input GeoTIFF files
top_tiff_file = path+'/top_dem.tif'
thickness_tiff_file = path+'/bed_dem.tif'

save_txt_file_top_dem=path+'/top_dem.dat'
save_txt_file_bottom_dem=path+'/bottom_dem.dat'
save_txt_contour=path+'/contour.dat'

replacement_value=-9999.0   # Value put in the final file were no value is given by the dem
scale_factor = 3         # Value of how much do we imporve quality
min_ice_thickness=1.5       # minimum ice thickness before considered as 0

kernel_size = (10,10)        # (width, height) of the kernel
sigma = 0

# Target CRS (EPSG:32632 - WGS 84 / UTM zone 32N)
dst_crs = 'EPSG:32632'

## reprojection o fthe geotiff

def reproject_geotiff(input_file, output_file, dst_crs):
    # Open the input GeoTIFF file
    with rasterio.open(input_file) as src:
        # Calculate the transform and dimensions of the reprojected raster
        transform, width, height = calculate_default_transform(
            src.crs, dst_crs, src.width, src.height, *src.bounds)

        # Update the metadata for the new projection
        kwargs = src.meta.copy()
        kwargs.update({
            'crs': dst_crs,
            'transform': transform,
            'width': width,
            'height': height
        })

        # Create the output file with the updated metadata
        with rasterio.open(output_file, 'w', **kwargs) as dst:
            for i in range(1, src.count + 1):
                # Reproject and write each band
                reproject(
                    source=rasterio.band(src, i),
                    destination=rasterio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=transform,
                    dst_crs=dst_crs,
                    resampling=Resampling.nearest)



# Reproject both GeoTIFF files
reproject_geotiff(top_tiff_file, top_tiff_file, dst_crs)
reproject_geotiff(thickness_tiff_file,thickness_tiff_file, dst_crs)

## convert geo tiff to text file

# bottom dem
with rasterio.open(top_tiff_file) as src:
    elevation_data = src.read(1)
    transform = src.transform
    rows, cols = elevation_data.shape
    x_coords = np.zeros((rows, cols))
    y_coords = np.zeros((rows, cols))
    for row in range(rows):
        for col in range(cols):
            x, y = rasterio.transform.xy(transform, row, col)
            x_coords[row, col] = x
            y_coords[row, col] = y

top_dem=np.array([x_coords,y_coords,elevation_data])
top_dem[2]=np.where(top_dem[2]==0, replacement_value, top_dem[2])

# thickness dem
with rasterio.open(thickness_tiff_file) as src:
    elevation_data = src.read(1)
    transform = src.transform
    rows, cols = elevation_data.shape
    x_coords = np.zeros((rows, cols))
    y_coords = np.zeros((rows, cols))
    for row in range(rows):
        for col in range(cols):
            x, y = rasterio.transform.xy(transform, row, col)
            x_coords[row, col] = x
            y_coords[row, col] = y
            if row+col<28 or row<5:
                elevation_data[row,col]=replacement_value
            if elevation_data[row,col]==0:
                elevation_data[row,col]=replacement_value


thickness_dem=np.array([x_coords,y_coords,elevation_data])

## creat contour

binary_image=np.where(thickness_dem[2]==replacement_value, 0, thickness_dem[2])

binary_image = np.where(binary_image != 0, 255, 0).astype(np.uint8)

contours, _ = cv2.findContours(binary_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
contour_points = contours[0].reshape(-1, 2)

contour=[]

for point in contour_points:
    contour.append([thickness_dem[0,point[1],point[0]],thickness_dem[1,point[1],point[0]]])
contour=np.array(contour)

x = contour[:, 0]
y = contour[:, 1]
tck, u = splprep([x, y], s=10)
new_points = splev(np.linspace(0, 1, 200), tck)
contour=np.array(new_points).T

## quality improvements

top_dem_rezise=np.zeros([3,len(top_dem[0])*scale_factor,len(top_dem[0,0])*scale_factor])

top_dem[2]=np.where(top_dem[2]==replacement_value, np.nan, top_dem[2])
top_dem_rezise[2]=cv2.resize(top_dem[2], None, fx=scale_factor, fy=scale_factor, interpolation=cv2.INTER_CUBIC)
top_dem_rezise[2]=cv2.blur(top_dem_rezise[2], kernel_size)
for row in range(np.shape(top_dem_rezise)[1]):
    for col in range(np.shape(top_dem_rezise)[2]):
        if np.isnan(top_dem_rezise[2][row,col]):
            top_dem_rezise[2][row,col]=replacement_value

top_dem_rezise[0]=np.array([np.linspace(top_dem[0,0,0],top_dem[0,0,-1],len(top_dem[0,0])*scale_factor)]*len(top_dem[0])*scale_factor)
top_dem_rezise[1]=np.array([np.linspace(top_dem[1].T[0,0],top_dem[1].T[0,-1],len(top_dem[1].T[0])*scale_factor)]*len(top_dem[1].T)*scale_factor).T



thickness_dem_rezise=np.zeros([3,len(thickness_dem[0])*scale_factor,len(thickness_dem[0,0])*scale_factor])

thickness_dem[2]=np.where(thickness_dem[2]==replacement_value, 0, thickness_dem[2])
thickness_dem_rezise[2]=cv2.resize(thickness_dem[2], None, fx=scale_factor, fy=scale_factor, interpolation=cv2.INTER_CUBIC)
thickness_dem_rezise[2]=cv2.blur(thickness_dem_rezise[2], kernel_size)
for row in range(np.shape(thickness_dem_rezise)[1]):
    for col in range(np.shape(thickness_dem_rezise)[2]):
        if np.isnan(thickness_dem_rezise[2][row,col]):
            thickness_dem_rezise[2][row,col]=replacement_value
        if thickness_dem_rezise[2,row,col] < min_ice_thickness:
            thickness_dem_rezise[2,row,col]=replacement_value

thickness_dem_rezise[0]=np.array([np.linspace(thickness_dem[0,0,0],thickness_dem[0,0,-1],len(thickness_dem[0,0])*scale_factor)]*len(thickness_dem[0])*scale_factor)
thickness_dem_rezise[1]=np.array([np.linspace(thickness_dem[1].T[0,0],thickness_dem[1].T[0,-1],len(thickness_dem[1].T[0])*scale_factor)]*len(thickness_dem[1].T)*scale_factor).T




## creation of the bottom dem

bottom_dem_rezise=np.zeros(np.shape(top_dem_rezise[0]))

for row in tqdm(range(np.shape(bottom_dem_rezise)[0])):
    for col in range(np.shape(bottom_dem_rezise)[1]):

        x=top_dem_rezise[0,row,col]
        y=top_dem_rezise[1,row,col]

        error=10e+15
        row_min=0
        col_min=0

        len_row_2=np.shape(thickness_dem_rezise[0])[0]
        len_col_2=np.shape(thickness_dem_rezise[0])[1]

        for  row_2 in range(len_row_2):
            for  col_2 in range(len_col_2):
                diff=(thickness_dem_rezise[0,row_2,col_2]-x)**2+(thickness_dem_rezise[1,row_2,col_2]-y)**2
                if diff<error:
                    error=diff
                    row_min=row_2
                    col_min=col_2

        if math.isclose( thickness_dem_rezise[2,row_min,col_min], replacement_value,abs_tol=10) or math.isclose( top_dem_rezise[2,row,col], replacement_value,abs_tol=1):
            bottom_dem_rezise[row,col]=replacement_value
        else :
            bottom_dem_rezise[row,col]=top_dem_rezise[2,row,col]-thickness_dem_rezise[2,row_min,col_min]


bottom_dem_rezise=np.array([top_dem_rezise[0],top_dem_rezise[1],np.where(bottom_dem_rezise==0,replacement_value,bottom_dem_rezise)])

## remove elements from top dem so that the 2 dem match

for row in range(np.shape(bottom_dem_rezise[0])[0]):
    for col in range(np.shape(bottom_dem_rezise[0])[1]):
        if bottom_dem_rezise[2][row,col]==replacement_value:
            top_dem_rezise[2][row,col]=replacement_value

## save dem to text file

f=open(save_txt_file_top_dem,"w")
for inv_row in range(np.shape(top_dem_rezise[0])[0]):
    for col in range(np.shape(top_dem_rezise[0])[1]):
        row=np.shape(top_dem_rezise[0])[0]-inv_row-1
        f.write(str(round(top_dem_rezise[0,row,col],2))+" "+str(round(top_dem_rezise[1,row,col],2))+" "+str(round(top_dem_rezise[2,row,col],2))+"\n")
f.close()

f=open(save_txt_file_bottom_dem,"w")
for inv_row in range(np.shape(bottom_dem_rezise[0])[0]):
    for col in range(np.shape(bottom_dem_rezise[0])[1]):
        row=np.shape(top_dem_rezise[0])[0]-inv_row-1
        f.write(str(round(bottom_dem_rezise[0,row,col],2))+" "+str(round(bottom_dem_rezise[1,row,col],2))+" "+str(round(bottom_dem_rezise[2,row,col],2))+"\n")
f.close()

f=open(save_txt_contour,"w")
for line in contour:
        f.write(str(round(line[0],2))+" "+str(round(line[1],2))+"\n")
f.close()

print("Things to modify in the elmer file \n")

print("\n Bed Dem")
print("x0 : "+str(round(bottom_dem_rezise[0,0,0],2)))
print("y0 : "+str(round(bottom_dem_rezise[1,-1,-1],2)))
print("lx : "+str(round(abs(bottom_dem_rezise[0,-1,-1]-bottom_dem_rezise[0,0,0]),2)))
print("ly : "+str(round(abs(bottom_dem_rezise[1,-1,-1]-bottom_dem_rezise[1,0,0]),2)))
print("Nx : "+str(np.shape(bottom_dem_rezise[0])[1]) )
print("Ny : "+str(np.shape(bottom_dem_rezise[0])[0]) )

print("\n Surface Dem")
print("x0 : "+str(round(top_dem_rezise[0,0,0],2)))
print("y0 : "+str(round(top_dem_rezise[1,-1,-1],2)))
print("lx : "+str(round(abs(top_dem_rezise[0,-1,-1]-top_dem_rezise[0,0,0]),2)))
print("ly : "+str(round(abs(top_dem_rezise[1,-1,-1]-top_dem_rezise[1,0,0]),2)))
print("Nx : "+str(np.shape(top_dem_rezise[0])[1]) )
print("Ny : "+str(np.shape(top_dem_rezise[0])[0]) )


plt.subplot(2, 2, 1)
plt.imshow(np.where(top_dem_rezise[2]==replacement_value, np.nan, top_dem_rezise[2]),label="top dem")
plt.colorbar()
plt.title("Top DEM (m)")
plt.axis('equal')

plt.subplot(2, 2, 2)
plt.imshow(np.where(thickness_dem_rezise[2]==replacement_value, np.nan, thickness_dem_rezise[2]),label="Thickness (m)")
plt.axis('equal')
plt.title("Ice thickness (m)")
plt.colorbar()

plt.subplot(2, 2, 3)
plt.imshow(np.where(bottom_dem_rezise[2]==replacement_value, np.nan, bottom_dem_rezise[2]),label="bottom_dem (m)")
plt.title("Bottom DEM (m)")
plt.axis('equal')
plt.colorbar()

plt.subplot(2, 2, 4)
plt.plot(contour[:, 0], contour[:, 1],label="contour",color="red")
plt.title("Glacier boundary")
plt.axis('equal')

plt.show()
##


