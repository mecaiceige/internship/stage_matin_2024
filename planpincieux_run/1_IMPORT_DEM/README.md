In this first step we will run a static simulation of the Planpincieux glacier 

Execute the simulation:<br>
Serial: 'ElmerSolver initialise_DEM.sif'<br> 
Parallel: `ElmerSolver initialise_DEM.sif`  

Take care that the number of nodes required (np) is compatible with the number of partitions of the mesh (4 here) 
