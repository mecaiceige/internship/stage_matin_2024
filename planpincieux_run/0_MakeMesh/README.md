Read a contour of the area around the Planpincieux glacier and build the `geo`input file that will be used to make the mesh
`python Contour2geo.py -r 150.0 -i ../data/contour.dat -o ARG_mesh.geo`<br>

You can change the resolution of the mesh directly by modifying the **lc** variable in pp_mesh.geo

Then make the Elmer mesh:<br>
`gmsh -1 -2 pp_mesh.geo -o pp_mesh.msh`<br>
`ElmerGrid 14 2 pp_mesh.msh -autoclean`<br> 

If going parallel:<br>
`ElmerGrid 14 2 pp_mesh.msh -autoclean -metis 4 0`<br> 

Create a vtu file to visualise the mesh:<br>
`ElmerGrid 14 5 pp_mesh.msh -autoclean -metis 4 0`<br>
`ElmerGrid 14 5 pp_mesh.msh -autoclean`<br>

At the end, the directory pp_mesh should be copied in all the directories:<br>
'cp -r pp_mesh ../1_IMPORT_DEM/'<br>



